package main

import (
    "net"
    "bufio"
    "os"
    "log"
    "fmt"
    "io"
    "bytes"
    "strconv"
    "os/exec"
)

var IPS = []string {
        "fa19-cs425-g05-01.cs.illinois.edu",
        "fa19-cs425-g05-02.cs.illinois.edu",
        "fa19-cs425-g05-03.cs.illinois.edu",
        "fa19-cs425-g05-04.cs.illinois.edu",
        "fa19-cs425-g05-05.cs.illinois.edu",
        "fa19-cs425-g05-06.cs.illinois.edu",
        "fa19-cs425-g05-07.cs.illinois.edu",
        "fa19-cs425-g05-08.cs.illinois.edu",
        "fa19-cs425-g05-09.cs.illinois.edu",
        "fa19-cs425-g05-10.cs.illinois.edu",
        // hostname list: machine01, machine02, ...
}

func getGrepPattern() string {
    // read grep pattern from stdin
    reader := bufio.NewReader(os.Stdin)
    fmt.Print("Grep Pattern: ")
    grepPattern, err := reader.ReadString('\n')
    if err != nil {
        log.Fatal(err, "\n")
    }
    return grepPattern
}


func request(grepPattern string, counter *Counter, host string, response chan string, id int) {
    var ip int
    fmt.Sscanf(host, "fa19-cs425-g05-%d.cs.illinois.edu", &ip)
    if (ip != id) {
        conn, err := net.Dial("tcp", host)
        if err != nil {
            log.Println(err)
            if (counter.check()) {
                close(response)
            }
            return
        }
        // send grep pattern
        fmt.Fprintf(conn, grepPattern + "\n")
        // receive grep result
        log.Printf("Requesting grep result from remote host: %s\n", host)

        var buf bytes.Buffer
        io.Copy(&buf, conn)
        response <- buf.String()
    } else {
        log.Printf("Requesting grep result from local host: %s\n", host)
        pattern := string(grepPattern[:len(grepPattern) - 1])
        pattern = "grep -HnE " + pattern + " vm" + strconv.Itoa(id) + ".log"
        log.Print("Pattern: ", pattern, "\n")
        cmd := exec.Command("/bin/bash", "-c", pattern)
        // cmd.Env = append(os.Environ(), "LC_ALL=C")
        stdoutStderr, err := cmd.CombinedOutput()
        if err != nil {
            log.Print(err, "\n")
        } else {
            response <- string(stdoutStderr)
        }
    }
    if (counter.check()) {
        close(response)
    }
}

func grepRequest(grepPattern string, port string, id int) chan string {
    counter := &Counter{val: 0, cap: len(IPS)}
    response := make(chan string, len(IPS))

    for _, hostname := range IPS {
        go request(grepPattern, counter, hostname + ":" + port, response, id)
    }
    return response
}

func client(port *string, debug *bool) {
    hostname, _ := os.Hostname()
    var id int
    fmt.Sscanf(hostname, "fa19-cs425-g05-%d.cs.illinois.edu", &id)
    if !(*debug) {
        // set up client log
        f, err := os.OpenFile(fmt.Sprintf("./client.%d.log", id), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
        if err != nil {
            log.Printf("Error opening file: %v", err)
        }
        defer f.Close()
        log.SetOutput(f)
    }
    grepPattern := getGrepPattern()
    response := grepRequest(grepPattern, *port, id)

    for resp := range response {
        fmt.Print(resp)
    }
}
    
