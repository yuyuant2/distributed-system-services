package main

import (
    "net"
    "bufio"
    "os/exec"
    "os"
    "log"
    "fmt"
    "strconv"
)

func grepResponse(conn *net.Conn, id int) {
    pattern, err := bufio.NewReader(*conn).ReadString('\n')
    pattern = string(pattern[:len(pattern) - 1])
    if err != nil {
        log.Print(err, "\n")
        return
    }

    log.Print("Grep Pattern Received: ", pattern, "\n")
    pattern = "grep -HnE " + pattern + " vm" + strconv.Itoa(id) + ".log"
    cmd := exec.Command("/bin/bash", "-c", pattern)
    stdoutStderr, err := cmd.CombinedOutput()
    if err != nil {
        log.Print(err, "\n")
    } else {
        // send new string back to client
        (*conn).Write(stdoutStderr)
    }
    (*conn).Close()
}

func server(port *string, debug *bool) {
    // create machine log
    hostname, _ := os.Hostname()
    var id int
    fmt.Sscanf(hostname, "fa19-cs425-g05-%d.cs.illinois.edu", &id)
    if !(*debug) {
        // set up server log
        f, err := os.OpenFile(fmt.Sprintf("./server.%d.log", id), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
        if err != nil {
            log.Printf("error opening file: %v", err)
        }
        defer f.Close()
        log.SetOutput(f)
    }
    
    log.Print("Launching server on ", *port)
    ln, err := net.Listen("tcp", ":" + *port)
    if err != nil {
        log.Fatal(err, "\n")
    }

    for {
        conn, err := ln.Accept()
        if err != nil {
            log.Print(err, "\n")
            continue
        }

        log.Println("Serving ", conn.RemoteAddr().String())
        go grepResponse(&conn, id)
    }
}