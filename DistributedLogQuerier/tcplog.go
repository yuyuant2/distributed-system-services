package main

import (
    "flag"
    "strings"
)
// https://opensource.com/article/18/5/building-concurrent-tcp-server-go
// https://www.thepolyglotdeveloper.com/2017/05/network-sockets-with-the-go-programming-language

func main() {
    mode := flag.String("m", "server", "Mode: server | client | createlog")
    port := flag.String("p", "8081", "Server listenning Port")
    debug := flag.Bool("d", false, "Debug: Print Log to Stdout or File")
    
    flag.Parse()
    if strings.ToLower(*mode) == "server" {
        server(port, debug)
    } else if strings.ToLower(*mode) == "createlog" {
        createLog()
    } else {
        client(port, debug)
    }
}