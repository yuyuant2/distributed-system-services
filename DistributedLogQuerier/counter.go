package main

import (
    "sync"
)

type Counter struct {
    val int
    cap int
    mux sync.Mutex
}

func (c *Counter) check() bool {
        c.mux.Lock()
        defer c.mux.Unlock()
        c.val++
        if c.val == c.cap {
                return true
        }
        return false
}