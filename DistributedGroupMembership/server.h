//
//  machine.h
//  DistributedGroupMembership
//
//  Created by Tangrizzly on 2019/9/27.
//  Copyright © 2019 Tangrizzly. All rights reserved.
//

#ifndef server_h
#define server_h

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <mutex>
#include <thread>
#include <sstream>
#include <cstring>
#include <netdb.h>
#include <vector>
#include <unistd.h>
#include "config.h"
#include "member.h"

class Server {
private:
    mutex mux;
//    ostream* ofs;
    string localIp;
    map<string, Member> memberships;
    enum GossipType {heartbeat, join, fail, intro, intro_rsp};
    bool running;
    int recvsockfd;
public:
    void init_membershiplist() {
        memberships[localIp] = Member();
        int sockfd;
        sockaddr_in addr;
        socklen_t addrlen = sizeof(addr);
        memset((char(*)) &addr, 0, sizeof(addr));
        if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            perror("Socket failed");
            return;
        }
        addr.sin_family = AF_INET;
        addr.sin_port = htons(PORT);
        
        for (unordered_set<string>::iterator it = introducers.begin(); it != introducers.end(); it++) {
            if (*it == localIp) {
                continue;
            }

            if (inet_aton((*it).c_str(), &addr.sin_addr) == 0) {
                perror("Assign ip failed");
                continue;
            }
            string msg = to_string(intro) + ';';
            if (sendto(sockfd, msg.c_str(), msg.length(), 0, (sockaddr*) &addr, sizeof(addr)) < 0) {
                perror("Send introduction failed");
                continue;
            }
            
            timeval tv;
            tv.tv_sec = 0;
            tv.tv_usec = T_OUT;
            if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
                perror("Setsockopt failed");
                continue;
            }
            
            // receive membership list
            char buffer[BUFFER_SIZE];
            ssize_t buflen;
            if ((buflen = recvfrom(sockfd, buffer, BUFFER_SIZE, 0, (sockaddr*) &addr, &addrlen)) < 0) {
                perror("Receive failed");
                cout << *it + ": Timeout reached." << endl;
                continue;
            }
            buffer[buflen] = '\0';
            cout << "Get introducer respond: " << *it << endl;
            msg = string(buffer);
            cout << "Received message \"" << msg << "\"" << endl;
            istringstream iss(msg);
            string mode;
            getline(iss, mode, ';');
            if (static_cast<GossipType>(stoi(mode)) != intro_rsp) {
                continue;
            }
            string hostname, joinstmp;
            while (getline(iss, hostname, ';')) {
                getline(iss, joinstmp, ';');
                memberships[hostname] = Member(stoll(joinstmp.c_str()));
            }
            print_membershipList();
            break;
        }
        close(sockfd);
        running = true;
    }
    
    Server(bool debug) {
        // get localIp
        char localhost_c[HOSTNAME_SIZE];
        gethostname(localhost_c, HOSTNAME_SIZE);
        
        int machine_id = stoi(string(localhost_c).substr(15, 2));
        char log_path_c[32];
        sprintf(log_path_c, LOG_PATH_FORMAT, machine_id);
//        fout = fout->open(string(log_path_c), ofstream::out);

        char *IPbuffer;
        hostent *host_entry;
        host_entry = gethostbyname(localhost_c);
        
        // To convert an Internet network
        // address into ASCII string
        IPbuffer = inet_ntoa(*((in_addr*) host_entry->h_addr_list[0]));
        localIp = string(IPbuffer);
        
        cout << "Host IP: " << localIp << endl;
        
        // TODO: add Exception
        init_membershiplist();
    }
    
    bool state() {
        return running;
    }
    
    void updateNeighbors() {
        map<string, Member>::iterator it = memberships.find(localIp);
        for (int i = 0; i < PREDECESSOR; i++) {
            if (it == memberships.begin()) {
                it = memberships.end();
            }
            --it;
            if (it->first == localIp) {
                break;
            }
            it->second.refreshHbstmp();
        }
    }
    
    void print_membershipList() {
        cout << "Membership list: ";
        cout << memberships.size() << endl;
        for (auto &it: memberships) {
            cout << it.first << ',' << it.second.getJoinstmp() << endl;
        }
    }
    
    void leave() {
        running = false;
        shutdown(recvsockfd, SHUT_RDWR);
        string msg = to_string(fail) + ';' + localIp + ';';
        mux.lock();
        gossip(msg);
        memberships.clear();
        print_membershipList();
        mux.unlock();
    }
    
    void gossip(string msg) {
        int sockfd;
        sockaddr_in addr;
        memset((char*) &addr, 0, sizeof(addr));
        if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            perror("Socket failed.");
            return;
        }
        addr.sin_family = AF_INET;
        addr.sin_port = htons(PORT);
        
        map<string, Member>::iterator it = memberships.find(localIp);
        for (int i = 0; i < SUCCESSOR; i++) {
            if (++it == memberships.end()) {
                it = memberships.begin();
            }
            if (it->first == localIp) {
                break;
            }
            if (inet_aton((it->first).c_str(), &addr.sin_addr) < 0) {
                perror("Assign ip failed");
                continue;
            }
            
            if (sendto(sockfd, msg.c_str(), msg.length(), 0, (sockaddr*) &addr, sizeof(addr)) < 0) {
                perror("Send heartbeat failed");
                continue;
            }
        }
        close(sockfd);
    }
    
    /*
     receive join, fail/leave, heartbeat, and self-introduction message
     
     join format:       GossipType.join;new_join_node_hostname;join_timestamp
     fail/leave format: GossipType.fail;failed_node_hostname
     heartbeat format:  GossipType.heartbeat;
     self-intro format: GossipType.intro;
     */
    void receive() {
        cout << "Setting up thread to receive messages." << endl;
        sockaddr_in addr, cltaddr;
        socklen_t cltaddrlen = sizeof(cltaddr);
        memset((char*) &addr, 0, sizeof(addr));
        if ((recvsockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            perror("Socket failed");
            return;
        }
        
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = htons(PORT);
        
        if (::bind(recvsockfd, (sockaddr*) &addr, sizeof(addr)) < 0) {
            perror("Bind failed");
            return;
        }
        
        char buffer[BUFFER_SIZE];
        while (running) {
            ssize_t buflen = recvfrom(recvsockfd, buffer, BUFFER_SIZE, 0, (sockaddr*) &cltaddr, &cltaddrlen);
            if (buflen <= 0) {
                break;
            }
            buffer[buflen] = '\0';
            string msg = string(buffer);
            istringstream iss(msg);
            string line;
            getline(iss, line, ';');
            GossipType mode = static_cast<GossipType>(stoi(line));
            string hostname = inet_ntoa(cltaddr.sin_addr);
            mux.lock();
            if (mode == heartbeat) {
//                cout << "Received message \"" << msg << "\": heartbeat from" << hostname << endl;
                if (memberships.find(hostname) != memberships.end()) {
                    memberships[hostname].refreshHbstmp();
                }
            } else if (mode == join) {
                getline(iss, line, ';');  // get hostname
                cout << "Received message \"" << msg << "\": join " << line << endl;
                if (memberships.find(line) == memberships.end()) {
                    memberships[hostname].refreshHbstmp();
                    cout << "New node joined." << endl;
                    string jointime;
                    getline(iss, jointime, ';');
                    memberships[line] = Member(stoll(jointime));
                    updateNeighbors();
                    gossip(msg);
                    print_membershipList();
                }
            } else if (mode == fail) {
                getline(iss, line, ';'); // get hostname
                cout << "Received message \"" << msg << "\": fail " << line << endl;
                if (memberships.find(line) != memberships.end()) {
                    if (line == localIp) {
                        cout << "False positive detection." << endl;
                    } else {
                        cout << "Old node failed." << endl;
                        gossip(msg);
                        memberships.erase(line);
                        updateNeighbors();
                        print_membershipList();
                    }
                }
            } else if (mode == intro) {
                cout << "Received message \"" << msg << "\": intro from " << hostname << endl;
//                if (memberships.find(hostname) == memberships.end()) {
                    cout << "Detected new node." << endl;
                    memberships[hostname] = Member();
                    updateNeighbors();
                    
                    string msg = to_string(intro_rsp) + ';';
                    for (auto &it: memberships) {
                        msg += it.first + ';' + to_string(it.second.getJoinstmp()) + ';';
                    }
                    cout << "Send back msg: \"" << msg << "\"" << endl;
                    if (sendto(recvsockfd, msg.c_str(), msg.length(), 0, (sockaddr*) &cltaddr, cltaddrlen) < 0) {
                        perror("Failed to send membershiplist");
                    }
                    gossip(to_string(join) + ';' + hostname + ';' + to_string(memberships[hostname].getJoinstmp()) + ';');
                    print_membershipList();
//                }
            } else {
                cout << "Unknown mode: " << mode << endl;
            }
            mux.unlock();
        }
        cout << "Stop receiving." << endl;
        close(recvsockfd);
    }
    
    // thread: send heartbeat
    void send_heartbeat() {
        cout << "Setting up thread to send heartbeat." << endl;
        while (running) {
            string msg = to_string(heartbeat) + ';';
            mux.lock();
            gossip(msg);
            mux.unlock();
            usleep(HEARTBEAT_FREQUENCY);
        }
        cout << "Stop sending heartbeats." << endl;
    }
    
    // thread: check crash
    void check_crash() {
        cout << "Setting up thread to check crash." << endl;
        while (running) {
            mux.lock();
            vector<string> toBeDeleted;
            map<string, Member>::iterator it = memberships.find(localIp);
            for (int i = 0; i < PREDECESSOR; i++) {
                if (it == memberships.begin()) {
                    it = memberships.end();
                }
                --it;
                if (it->first == localIp) {
                    break;
                }
                long long curTime = it->second.getCurrtime();
                if (curTime - it->second.getHbstmp() > T_FAIL) {
                    cout << "Detected node failed: " << (it->first) << endl;
                    // gossip failed
                    toBeDeleted.push_back(it->first);
                }
            }
            if (toBeDeleted.size() > 0) {
                for (string& s: toBeDeleted) {
                    memberships.erase(s);
                }
                updateNeighbors();
                print_membershipList();
                for (string& ip: toBeDeleted) {
                    string msg = to_string(fail) + ';' + ip + ';';
                    gossip(msg);
                }
            }
            mux.unlock();
            usleep(CHECK_FREQUENCY);
        }
        cout << "Stop checking crash." << endl;
    }
};

#endif /* server_h */
