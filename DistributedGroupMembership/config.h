//
//  config.cpp
//  DistributedGroupMembership
//
//  Created by Tangrizzly on 2019/9/22.
//  Copyright © 2019 Tangrizzly. All rights reserved.
//

#ifndef config_h
#define config_h

#include <string>
#include <unordered_set>

using namespace std;

unordered_set<string> introducers { // 1 4 5 8 9
    "172.22.154.15", // 4
    "172.22.156.15", // 8
    "172.22.152.20", // 1
    "172.22.154.16", // 5
    "172.22.156.16", // 9
    "172.22.152.21", // 2
    "172.22.154.17", // 6
    "172.22.156.17", // 10
    "172.22.152.22", // 3
    "172.22.154.18"  // 7
};

#define LOG_PATH_FORMAT "../log/groupMembership%d.log"
#define PREDECESSOR 3 // heartbeat monitored
#define SUCCESSOR 3 // heartbeat receiver
#define PORT 8083
// TODO: reset time
#define HEARTBEAT_FREQUENCY 500000 // us = 0.500000s
#define CHECK_FREQUENCY 600000 // us = 0.600000s
#define T_FAIL 1600 // ms
#define BUFFER_SIZE 2048
#define T_OUT 100000 // us
#define HOSTNAME_SIZE 64

#endif /* config_h */
