//
//  main.cpp
//  DistributedGroupMembership
//
//  Created by Tangrizzly on 2019/9/22.
//  Copyright © 2019 Tangrizzly. All rights reserved.
//

#include <iostream>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <fstream>
#include <map>
#include "server.h"

int main(int argc, const char * argv[]) {
    bool debug = false;
    if (argc >= 2 && (strcmp(argv[1], "d") == 0)) {
        cout << "debug" << endl;
        debug = true;
    }

    Server server(debug);

    thread receive_msg(&Server::receive, &server);
    thread send_hb(&Server::send_heartbeat, &server);
    thread check_crsh(&Server::check_crash, &server);

    string str;
    while (getline(cin, str)) {
        if (str == "leave" && server.state()) {
            server.leave();
            check_crsh.join();
            receive_msg.join();
            send_hb.join();
        } else if (str == "join" && !server.state()) {
            server.init_membershiplist();
            receive_msg = thread(&Server::receive, &server);
            send_hb = thread(&Server::send_heartbeat, &server);
            check_crsh = thread(&Server::check_crash, &server);
        }
    }

    return 0;
}
