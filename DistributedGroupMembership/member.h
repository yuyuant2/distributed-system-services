//
//  member.h
//  DistributedGroupMembership
//
//  Created by Tangrizzly on 2019/9/27.
//  Copyright © 2019 Tangrizzly. All rights reserved.
//

#ifndef member_h
#define member_h

#include <chrono>

class Member {
private:
    long long joinstmp;
    long long hbstmp;
public:
    long long getCurrtime() {
        return chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
    }
    
    Member(long long _joinstmp):joinstmp(_joinstmp) {
        hbstmp = getCurrtime();
    }
    
    Member() {
        joinstmp = getCurrtime();
    }
    
    long long getJoinstmp() {
        return joinstmp;
    }
    
    long long getHbstmp() {
        return hbstmp;
    }
    
    void refreshHbstmp() {
        hbstmp = getCurrtime();
    }
};

#endif /* member_h */
