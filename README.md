
# Distributed System Services

[TOC]

## Distributed Log Querier

A Go-based multi-threading program to query log files among distributed systems for debugging distributed services.

### Usage

1. Copy the folder  `DistributedLogQuerier` into your `$GOPATH`
2. Install the tcplog package on each machine by `go install tcplog`
3. Set up server process on each machine by `tcplog`
4. Grep distributed log from any machine by `tcplog -m="client"`
5. For debug mode, `-d` can be append to the tcplog command

### Design

For server, we use a for loop to keep accepting requests. Upon receiving a request, it starts a thread which greps the requested pattern from the log file on the server and sends back the results. For each grep client, we first read the pattern from standard input. Then, we start a thread to connect to each of the 10 remote machines, send the grep pattern, and receive grep results. In particularly, when the requested machine is the local machine, the thread will directly run the grep command locally instead of requesting the remote server. Additionally, we use a counter to count the machines who has sent back the results and who we fail to connect to. Once the counter reaches 10, grep will end.

## Distributed Group Membership

A distributed group membership service, maintaining group membership lists with scalable failure detection and gossip dissemination. It can ensure completeness for 3 simultaneous failures and low bandwidth usage.

### Usage

1. Cd into the folder `DistributedGroupMembership`
2. Compile the c++ code by `make`
3. Run the program on each machine by `./main`
4. To voluntarily leave the group, type `leave`, and to rejoin the group, type `join`
5. To simulate a machine failure, use `Ctrl-C`. 